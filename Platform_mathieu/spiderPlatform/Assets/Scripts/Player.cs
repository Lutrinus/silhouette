﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    private Vector2 axisVector;
    private Vector2 objectVector;
    public Rigidbody2D rb2d;

	// Use this for initialization
	void Start () {
        rb2d = GetComponent<Rigidbody2D>();

    }
	
	// Update is called once per frame
	void Update () {
        axisVector = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        objectVector = new Vector2(Mathf.Cos(this.transform.rotation.x), Mathf.Sin(this.transform.rotation.y));
        this.rb2d.velocity = axisVector * new Vector2(1,0) * objectVector;
	}
}
