﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {




	//TOOLTIPS FOR ALL PROTOTYPING VARIABLES!!!
	//KEEP IT PRETTY, girls


	[Tooltip ("scales player walking speed")]
	public float walkSpeed = 1.0f;

    [Tooltip("scales player crouching speed")]
    public float crouchSpeed = 0.5f;
    public float crouchCoolDown = 0.5f;

    [Tooltip ("the amount of force added at each impulse")]
	public float jumpForce = 100.0f;

    [Tooltip("countdown - if not 0 the player cannot jump")]
    public float jumpCoolDown = 0.2f;

    [Tooltip("controls available moves, for a given spacial situation")]
    public string localState = "onGround";

    [Tooltip("controls how the player is able to move")]
    public string motionState = "walk";

    

    //Utility
    private Rigidbody2D rb2d;
    private float currentSpeed;
    private float jumpCD;
    private float crouchCD;



    // Use this for initialization
    void Start () {

        rb2d = this.gameObject.GetComponent<Rigidbody2D>();
        currentSpeed = walkSpeed;
        jumpCD = jumpCoolDown;
        crouchCD = crouchCoolDown;


}
	
	// Update is called once per frame
	void Update () {
       
  
	
	}


	//ALWAYS USE PHYSICS!!!
    	void FixedUpdate()
	{
        Crouch();
        Move ();
    }

 
	//GENERAL PLAYER MOVEMENT [left,right,jump,crouch]
    void Crouch()
    {
        if(Input.GetAxis("Crouch") >= .5 && crouchCD == 0 && motionState != "crouch")
        {
            //Scale player to crouch size and reposition him
            gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x, 1.0f, gameObject.transform.localScale.z);
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 0.05f, gameObject.transform.position.z);

            //Change currentSpeed to crouch speed
            currentSpeed = crouchSpeed;

            //Set motion state and CD
            motionState = "crouch";
            crouchCD = crouchCoolDown;
        }
        else if (Input.GetAxis("Crouch") <= .1  && crouchCD == 0 && motionState == "crouch")
        {
            //Scale player to walk size
            gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x, 2.3f, gameObject.transform.localScale.z);
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 0.05f, gameObject.transform.position.z);

            //Change currentSpeed to walk speed
            currentSpeed = walkSpeed;

            //Set motion state
            motionState = "walk";
        }
    }
	void Move()
	{
        //Update CDs
        //JUMP
        if (jumpCD > 0)
        {
            jumpCD -= Time.deltaTime;
        }
        else if (jumpCD < 0)
        {
            jumpCD = 0;
        }
        //CROUCH
        if (crouchCD > 0)
        {
            crouchCD -= Time.deltaTime;
        }
        else if (crouchCD < 0)
        {
            crouchCD = 0;
        }

        //Adapt rotation
        RaycastHit2D rc2d = Physics2D.Raycast(this.transform.position, Vector2.down);
        Debug.DrawRay(this.transform.position, Vector2.down);
        Vector2 globalNormal = new Vector2(Mathf.Cos(Mathf.Deg2Rad*rc2d.transform.rotation.z),Mathf.Sin(Mathf.Deg2Rad * rc2d.transform.rotation.z));
        Debug.DrawRay(this.transform.position, globalNormal);
        this.transform.rotation = new Quaternion(this.transform.rotation.x, this.transform.rotation.y, Vector2.Angle(Vector2.left,rc2d.normal) , this.transform.rotation.w);

        //Walk
		rb2d.velocity = new Vector2(Input.GetAxis("Horizontal") * currentSpeed , rb2d.velocity.y);

        //Jump
		if(Input.GetAxis("Jump")>0 && jumpCD == 0 && localState == "onGround")
		{
            jumpCD = jumpCoolDown;
			rb2d.AddForce(new Vector2(0,jumpForce));
		}
			
	}

}
