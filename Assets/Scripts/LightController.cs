﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour {

	[Tooltip("Die wichtigste Variable im Spiel! Ist das Licht an oder aus?")]
	public bool areOn = true;
    [Tooltip("Die Farbe, die der Hintergrund hat, wenn das Licht an ist")]
    public Color backgroundLight;
    [Tooltip("Die Farbe, die der Hintergrund hat, wenn das Licht aus ist")]
    public Color backgroundDark;

	public bool onOff = true;
	[Tooltip("Das maximale intervall zwischen ein und aus, um als Flackern zu gelten in ms")]
	public float maxTime = 0.1f;

	//Utility Variablen für die Flicker Funktion
	private float switchTime = 0.0f;
	private int switchCount = 0;
	private bool count = false;

    [Tooltip("Die Farbe, die der Hintergrund hat, wenn das Licht aus ist")]
    public float coolDown = 0f;

    // Use this for initialization
    void Start () {
        UpdateLight();
	}
	
	// Update is called once per frame
	void Update () {


		Debug.Log (switchTime);
		Debug.Log (switchCount);
		Debug.Log (count);

		if(coolDown > 0)
        {
            coolDown -= Time.deltaTime;
        }
        else if (coolDown < 0)
        {
            coolDown = 0;
        }


		if (count) {
			switchTime += Time.deltaTime;

			if (count && switchTime < maxTime) {
				Flicker ();
			
			}
		}
		if (switchTime > maxTime) {
		
			switchTime = 0;
			count = false;
		}

    }

    void UpdateLight()
    {
        if (areOn)
        {
            GameObject.Find("bgGradient").GetComponent<Renderer>().enabled = false;
            Camera.main.backgroundColor = backgroundLight;
        }
        else
        {
            GameObject.Find("bgGradient").GetComponent<Renderer>().enabled = true;
            Camera.main.backgroundColor = backgroundDark;
        }
    }



	//Schaltet um.
	void Switch(){

        if(coolDown == 0)
        {
            areOn = !areOn;
            coolDown = .2f;
            UpdateLight();
        }

		onOff = !onOff ; 	
		count = true;

	}

	void CoolDown()
	{
		
	}


	//"Flackern" Aktion zum vertreiben von Schatten
	void Flicker(){
	

		count = false;
		switchTime = 0;
		switchCount = 0;

	
	}
}
