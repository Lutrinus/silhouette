﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformScript : MonoBehaviour {

    private GameObject Player;
    private LightController Lights;
    private SpiderTableBehavior EnemyCore;

	// Use this for initialization
	void Start () {
        Player = GameObject.Find("Player");
        Lights = GameObject.Find("GlobalLightManager").GetComponent<LightController>();



    }
	
	// Update is called once per frame
	void Update () {
        //PLATFORM PROPERTY: jump through bottom, stay on top | IgnoreCollision(obj1,obj2,bool)
        float dy = Player.transform.position.y - 0.06f * Player.transform.localScale.y;
        bool above = (dy < this.transform.position.y);
        Debug.DrawRay(new Vector2(Player.transform.position.x,dy), Vector2.left);
        Physics2D.IgnoreCollision(this.GetComponent<Collider2D>(), Player.GetComponent<Collider2D>(), above);

        if (Lights.areOn)
        {
            gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().color = Color.black;
        }


    }
}
