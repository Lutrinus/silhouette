﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour
{


    [Tooltip("Text-Objekt im Canvas hier reindraggen!")]
    public Text derText;
    [Tooltip("das hier verändern, um einen anderen Text auszugeben")]
    public string textString;

    // Use this for initialization
    void Start()
    {
        derText.text = textString;
    }

    // Update is called once per frame
    void Update()
    {
        derText.text = textString;

    }
}
