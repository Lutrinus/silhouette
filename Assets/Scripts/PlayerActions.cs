﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActions : MonoBehaviour {



	//Zum Überprüfen, ob die Taste schon auf an geschalten wurde.
	//Beim Einbau von Lichtschalter ändern! 
	private bool wasPressed = false;
	private bool firstPress = false;
    private PlayerMovement playerMovement;



	// Use this for initialization
	void Start () {
		playerMovement = gameObject.GetComponent<PlayerMovement>();
	}
	
	// Update is called once per frame
	void Update () {
		Flick ();
	}



	void Flick(){
		if (Input.GetAxis("Switch") >= .5) {
			GameObject.Find ("GlobalLightManager").SendMessage ("Switch");
		}
	}
 
    void Die()
	{
		GameObject.Find ("GUI").GetComponent<GUIManager>().derText.color = Color.red;
		GameObject.Find ("GUI").GetComponent<GUIManager>().textString = "YOU DIED";
		Destroy (this.gameObject);
	}

	void OnTriggerEnter2D (Collider2D coll)
    {
		if (coll.gameObject.tag == "Enemy" || coll.gameObject.tag == "WhiteGuy") {
            Die();
		}
		if (coll.gameObject.tag == "Door") {
			Win ();
		}
       
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Ground")
        {
            playerMovement.localState = "onGround";
        }
    }

    void OnCollisionStay2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Ground" || coll.gameObject.tag == "Default")
        {
            playerMovement.localState = "onGround";
        }

        if(coll.gameObject.tag == "Shadow" && (Input.GetKeyDown(KeyCode.S)))
        {
            gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x, 1.0f, gameObject.transform.localScale.z);
        }
        else
        {

            gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x, 2.3f, gameObject.transform.localScale.z);
        }
    }

    void OnCollisionExit2D(Collision2D coll)
    {
        if(coll.gameObject.tag == "Ground" || coll.gameObject.tag == "Default")
        {
            playerMovement.localState = "inAir";
        }
    }

    void Win()
	{
		GameObject.Find ("GUI").GetComponent<GUIManager>().derText.color = Color.green;
		GameObject.Find ("GUI").GetComponent<GUIManager>().textString = "YOU WIN";
	}
}
