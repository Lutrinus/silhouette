﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {


	[Tooltip("Die Geschwindigkeit des Gegners")]
	public float speed = 1.0f;


	private Rigidbody2D meinRigidbody;
	private Vector2 playerPos;
	private GameObject Lights;
    private LightController LightState;

	// Use this for initialization
	void Start () {
		meinRigidbody = this.GetComponent<Rigidbody2D> ();
		playerPos = GameObject.Find ("Player").GetComponent<Transform> ().position;
		Lights = GameObject.Find ("GlobalLightManager");
        LightState = Lights.GetComponent<LightController>();
	}
	
	// Update is called once per frame
	void Update () {

		if (Lights.GetComponent<LightController> ().areOn) {
			this.gameObject.GetComponent<SpriteRenderer> ().enabled = false;
		} else {
			this.gameObject.GetComponent<SpriteRenderer> ().enabled = true;
		
		}

		playerPos = GameObject.Find ("Player").GetComponent<Transform> ().position;		

	}



	//Immer für Physikberechnungen benutzen!
	void FixedUpdate()
	{
		if (Lights.GetComponent<LightController> ().areOn == false) {
			
			this.gameObject.GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.None;
			MoveTowardPlayer ();

		} else {

			this.gameObject.GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezePosition;

		}

	}


	// simple Bewegung. berücksichtigt weder Gravitation noch Sprünge usw.

	void MoveTowardPlayer()
	{
		meinRigidbody.velocity = (playerPos - (Vector2)this.gameObject.GetComponent<Transform> ().position).normalized * speed;
	}
}
