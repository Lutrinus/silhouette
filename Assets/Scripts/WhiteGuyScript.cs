﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteGuyScript : MonoBehaviour {

	[Tooltip("Die Geschwindigkeit beim Patroullieren")]
	public float walkSpeed = 1.0f;
	[Tooltip("Die Geschwindigkeit beim auf den Spieler zu rennen") ]
	public float dashSpeed = 5.0f;

	//-1 = links, 1 = rechts
	private int direction = 1;
	private RaycastHit2D hit;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		hit = Physics2D.Raycast (new Vector2(this.transform.position.x+direction*1, this.transform.position.y), new Vector2 (direction, 0));

		Debug.DrawRay (this.transform.position, new Vector2 (direction, 0),Color.red,5.0f);

		//Wenn der Spieler gesehen wird, renne zu ihm, wenn nicht, patroulliere.
		if (hit.collider.gameObject == GameObject.Find ("Player") &&
			GameObject.Find ("GlobalLightManager").GetComponent<LightController> ().areOn == true){
		
			Dash ();
            gameObject.GetComponent<SpriteRenderer>().color = new Color(250f/255f, 0f, 0f, 1f);
		} else {
			Patrol ();
            gameObject.GetComponent<SpriteRenderer>().color = new Color(150f/255f, 62/255f, 62/255f, 1f);
        }
	}

	void Patrol(){
	
		this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (direction * walkSpeed,0);
	
	}

	void Dash()
	{
		this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (direction * dashSpeed,0);
	}



	//Umdrehen, wenn er gegen eine Wand läuft
	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.tag == "Wall") {
		
			direction = -direction;	

		}
	}

}


