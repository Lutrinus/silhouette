﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderTableBehavior : MonoBehaviour
{
    ///////////////////////////////   CLASS DECLARATION   ///////////////////////////////////////////////////////////

    //PUBLIC VARIABLES - FOR EXTERNEAL PROTOTYPING
    [Tooltip("Die Geschwindigkeit, mit der die Plattform sich bewegt")]
    public float speed = 1.0f;

    [Tooltip("Die Strecke, die die Plattform zurücklegt bevor sie umdreht")]
    public float walkLength = 500.0f;

    [Tooltip("Die Höhe in Unity-units, die die Plattform im Hellen hat")]
    public float minHeight = 0.15f;

    [Tooltip("Die Höhe in Unity-units, die die Plattform im Dunklen hat")]
    public float maxHeight = 0.5f;

    [Tooltip("States that contol animation and motion behavior")]
    public string nextAction;

    [Tooltip("This state controls general behavior and apperance [monster/table].")]
    public bool creature = false;

    [Tooltip("This state controls whether it stalks the player or not.")]
    public bool stalk = false;



    //PRIVATE VARIABLES - FOR INTERNAL UTILITY
    private GameObject Player;
    private GameObject LeftLeg_Spider;
    private GameObject RightLeg_Spider;
    private GameObject LeftLeg_Normal;
    private GameObject RightLeg_Normal;
    private GameObject TableShadow;

<<<<<<< HEAD
    public List<GameObject> attachment;

    private Rigidbody2D rb2d;
=======
    private List<GameObject> light_only;
    private List<GameObject> dark_only;
>>>>>>> 8aca9ab118ca5e19b3d7e5ce1309915a562f5361

    private bool initAnim;
    private bool invert = true;

    private Rigidbody2D rb2d;
    private List<GameObject> legs;
    private AnimationState animState;

    private LightController Lights;
    
    //Utility
    bool DoubleListed(List<GameObject> list,GameObject new_element)
    {
        foreach(GameObject exisiting_element in list)
        {
            if(exisiting_element.name == new_element.name)
            {
                return true;
            }
        }
        return false;
    }


    ///////////////////////////////   UNITY ENGINE CALLS   ///////////////////////////////////////////////////////////

 

<<<<<<< HEAD
    void Walk()
    {
        rb2d.bodyType = RigidbodyType2D.Static;
        animator = transform.Find("SpiderLeg_L").GetComponent<Animator>();
        animator.Play("LiftLeg", 0, 0);
        animator = transform.Find("SpiderLeg_R").GetComponent<Animator>();
        animator.Play("LiftLeg", 0, 0);

    }

    // Use this for initialization
=======
>>>>>>> 8aca9ab118ca5e19b3d7e5ce1309915a562f5361
    void Start()
    {
        //get external game objects
        Lights = GameObject.Find("GlobalLightManager").GetComponent<LightController>();
        Player = GameObject.Find("Player");
<<<<<<< HEAD

        legs.Add(new List<GameObject>() normal);
        legs.Add(new List<GameObject>());
        legs
=======
>>>>>>> 8aca9ab118ca5e19b3d7e5ce1309915a562f5361
        rb2d = gameObject.GetComponent<Rigidbody2D>();

        light_only = new List<GameObject>();
        dark_only = new List<GameObject>();
        legs = new List<GameObject>();

        //get swapable gameobjects
        UpdateChildren();
        UpdateFigure();
    }


    // Update is called once per frame
    void Update()
    {
        //DEVELOPER FUNCTION
        if (Input.GetKey(KeyCode.Return)) ;
        {
            Debug.Log("updated childen");
            UpdateChildren();
        }
        //UPDATE CREATURE
        if (Lights.areOn && creature)
        {
            creature = false;
            rb2d.bodyType = RigidbodyType2D.Dynamic;
        } 
        else if (!Lights.areOn && !creature) 
        {
            initAnim = true; //enables first animation to transform
            this.nextAction = "transformToSpider";
            creature = true;
        }
        //UPDATE ANIMATION STATE

        UpdateAnimation();
        UpdateFigure();
    }

    ////////////////////////////////   APEARENCE   ///////////////////////////////////////////////////////////

    void UpdateFigure()
    {
      // show/hide in light/dark
        foreach (GameObject obj in dark_only)
        {
            obj.SetActive(!Lights.areOn);

        }
        foreach (GameObject obj in light_only)
        {
            obj.SetActive(Lights.areOn);
        }

        //change color
 
    }

    void UpdateChildren()
    {
        foreach (Transform child_tramsform in transform)
        {
            GameObject child = child_tramsform.gameObject;

            if (child.name[0] == '+' && !DoubleListed(light_only, child))
            {
                light_only.Add(child);
            }

            else if (child.name[0] == '-' && !DoubleListed(light_only, child))
            {
                dark_only.Add(child);
            }

<<<<<<< HEAD
                case "standUp":
                    motionState = "walkLead";
                    Walk();
                    break;

                case "walkLead":
                    motionState = "walkFollow";
                    rb2d.bodyType = RigidbodyType2D.Dynamic;
                    break;

                case "walkFollow":
                    motionState = "standing";
                    Walk();
=======
            if (child.GetComponent<LimbController_SpiderLeg>() != null)
            {
                legs.Add(child);
            }
        }

    }


    ////////////////////////////////   ANIMATION CONTROLL   ///////////////////////////////////////////////////////////
    void UpdateAnimation()
    {
        if (legs[0].GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.1f || initAnim)
        {
            initAnim = false;
            switch (nextAction)
            {
                case "transformToSpider":
                    rb2d.bodyType = RigidbodyType2D.Dynamic;
                    TransformToSpider();
                    nextAction = "standUp";                    
                    break;

                case "transformToTable":
                    rb2d.bodyType = RigidbodyType2D.Dynamic;
                    TransfromToTable();
                    nextAction = "transformToSpider";
                    break;

                case "standUp":
                    rb2d.bodyType = RigidbodyType2D.Dynamic;
                    StandUp();
                    nextAction = "walk";
                    break;

                case "walk":
                    //rb2d.bodyType = RigidbodyType2D.Static;
                    Walk(invert);
                    invert = !invert;
                    nextAction = "standUp";
                    break;

                case "stand":
                    rb2d.bodyType = RigidbodyType2D.Dynamic;
>>>>>>> 8aca9ab118ca5e19b3d7e5ce1309915a562f5361
                    break;
            }

        }
        UpdateFigure();

    }


    ////////////////////////////////   ANIMATION CALLS   ///////////////////////////////////////////////////////////

    void TransformToSpider()
    {
        foreach (GameObject leg in legs)
        {
            leg.GetComponent<Animator>().Play("TransformToSpider", 0, 0);
        }
    }

    void TransfromToTable()
    {
        foreach (GameObject leg in legs)
        {
            leg.GetComponent<Animator>().Play("TransformToTable", 0, 0);
        }
    }

    void StandUp()
    {
        foreach (GameObject leg in legs)
        {
            leg.GetComponent<Animator>().Play("StandUp", 0, 0);
        }
    }

    void Walk(bool invert)
    {
        foreach (GameObject leg in legs)
        {
            if (leg.GetComponent<LimbController_SpiderLeg>().leftLeg)
            {
                leg.GetComponent<Animator>().Play("LiftLeg", 0, 0);
            }
            else if (!leg.GetComponent<LimbController_SpiderLeg>().leftLeg)
            {
               leg.GetComponent<Animator>().Play("LiftLeg", 0, 0);
            }
        }
    }


}



