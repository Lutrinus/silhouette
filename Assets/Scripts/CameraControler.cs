﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControler : MonoBehaviour {

    public float zoomOut;
    public float zoomIn;

    private LightController Lights;
    private Camera Cam;
    private GameObject Player;
    private float animState;

    //Uitlity
    float Func(float t)
    {
        List<float> p = new List<float>
        {
            1.000f,
            0.375f,
            0.625f,
            0.005f
        };
        float f = (1 - t) * (1 - t) * (1 - t) * p[0] + 3 * t * (1 - t) * (1 - t) * p[1] + 3 * t * t * (1 - t) * p[2] + t * t * t * p[3];
        return (f);
    }


	// Use this for initialization
	void Start () {
        Lights = GameObject.Find("GlobalLightManager").GetComponent<LightController>();
        Cam = gameObject.GetComponent<Camera>();
        Player = GameObject.Find("Player");

        animState = 0f;
    }
	
	// Update is called once per frame
	void Update () {
        if (Lights.areOn && Cam.orthographicSize < 2f)
        {
            animState += 0.01f;
        }
        else if (!Lights.areOn && Cam.orthographicSize > 1.1f)
        {

            animState -= 0.01f;
        }

        float f = animState - 2;
        if (f < 0) f = 0;

        Cam.orthographicSize = f * (zoomIn - zoomOut) + zoomOut;
        transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y+1, -1);
	}
}
