﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyLocation : MonoBehaviour {

    public GameObject target;

	// Use this for initialization
	void Start () {
        this.transform.position = target.transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        this.transform.position = target.transform.position;        
    }
}
